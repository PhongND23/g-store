package com.saleweb.services;

import com.saleweb.common.SearchDTO;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.order.OrderCreateDTO;
import com.saleweb.models.order.OrderDTO;
import com.saleweb.models.product.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderService {
    void payment(OrderCreateDTO dto) throws ApplicationException;

    OrderDTO getOrder(Long id);

    List<OrderDTO> getAll(SearchDTO dto);

    Page<OrderDTO> getAllByStatus(Pageable pageable,Long id);

    void changeStatus(Long id,Long statusId);

    void buyNow(Long accountId,Long productId,Integer quantity,Long paymentId) throws ApplicationException;

    Page<OrderDTO> ordersPage(SearchDTO dto,Pageable pageable);

    void cancelOrder(Long orderId,Long accountId);

    List<OrderDTO> getOrdersBySeller(Long accountId);


}
