package com.saleweb.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Account;
import com.saleweb.domains.Image;
import com.saleweb.domains.Product;
import com.saleweb.domains.ProductCategory;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.product.ProductCreateDTO;
import com.saleweb.models.product.ProductDTO;
import com.saleweb.models.product.ProductUpdateDTO;
import com.saleweb.repositories.*;
import com.saleweb.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

  private final ProductRepository repository;
  private final ProductCategoryRepository productCategoryRepository;
  private final OrderRepository orderRepository;
  private final ObjectMapper mapper;
  private final ImageRepository imageRepository;
  private final AccountRepository accountRepository;

  @Override
  @Transactional
  public void create(ProductCreateDTO dto) throws ApplicationException {
    Product product = new Product(dto);
    repository.save(product);
    if (CollectionUtils.isEmpty(dto.getImages())) {
      throw new ApplicationException("", "Yêu cầu ít nhất 1 ảnh sản phẩm");
    }
    List<Image> images = new ArrayList<>();
    dto.getImages()
        .forEach(
            v -> {
              var image = Image.builder().productId(product.getId()).content(v).build();
              images.add(image);
            });
    imageRepository.saveAll(images);
  }

  @Override
  @Transactional
  public void update(ProductUpdateDTO dto, Long id) throws ApplicationException {
    Optional<Product> productOptional = repository.findById(id);
    if (productOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    //    if (StringUtils.isBlank(dto.getName())
    //        || StringUtils.isBlank(dto.getTitle())
    //        || dto.getImages().isEmpty()
    //        || Objects.isNull(dto.getCategoryId())
    //        || Objects.isNull(dto.getQuantity())
    //        || StringUtils.isBlank(dto.getProductStatus())
    //        || Objects.isNull(dto.getRootPrice())
    //        || Objects.isNull(dto.getPrice())
    //        || StringUtils.isBlank(dto.getAddress())
    //        || StringUtils.isBlank(dto.getDescription())) {
    //      throw new ApplicationException("", "Không được để trống các trường bắt buộc !");
    //    }

    var product = productOptional.get();
    if (CollectionUtils.isEmpty(dto.getImages())) {
      throw new ApplicationException("", "Yêu cầu ít nhất 1 ảnh sản phẩm");
    }
    imageRepository.deleteAllByProductId(product.getId());

    List<Image> images = new ArrayList<>();
    dto.getImages()
        .forEach(
            v -> {
              var image = Image.builder().productId(product.getId()).content(v).build();
              images.add(image);
            });
    imageRepository.saveAll(images);
    change(product, dto);
    repository.save(product);
  }

  private void change(Product product, ProductUpdateDTO dto) {
    product.setName(dto.getName());
    product.setUnit(dto.getUnit());
    product.setCategoryId(dto.getCategoryId());
    product.setPrice(dto.getPrice());
    product.setPromotionPrice(dto.getPromotionPrice());
    product.setQuantity(dto.getQuantity());
    product.setDescription(dto.getDescription());
    product.setStatus(dto.getStatus());
    product.setShortDescription(dto.getShortDescription());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    Optional<Product> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }

    repository.delete(byId.get());
  }

  @Override
  public ProductDTO findById(Long id) throws ApplicationException {
    return null;
  }

  @Override
  public Page<ProductDTO> getAll(SearchDTO dto, Pageable pageable) throws JsonProcessingException {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Product> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getName())) {
            predicates.add(criteriaBuilder.like(root.get("name"), "%" + dto.getName() + "%"));
          }
          if (!Objects.isNull(dto.getCategoryId())) {
            predicates.add(criteriaBuilder.equal(root.get("categoryId"), dto.getCategoryId()));
          }

          if (!Objects.isNull(dto.getId())) {
            predicates.add(criteriaBuilder.equal(root.get("id"), dto.getId()));
          }

          if (!Objects.isNull(dto.getAccountId())) {
            predicates.add(criteriaBuilder.equal(root.get("accountId"), dto.getAccountId()));
          }

          if (!Objects.isNull(dto.getStatus())) {
            predicates.add(criteriaBuilder.equal(root.get("status"), dto.getStatus()));
          }

          if (!Objects.isNull(dto.getFromPrice())) {
            predicates.add(
                criteriaBuilder.greaterThanOrEqualTo(root.get("price"), dto.getFromPrice()));
          }
          if (!Objects.isNull(dto.getToPrice())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"), dto.getToPrice()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Product> all = repository.findAll(specification, pageable);
    var products = all.getContent();
    List<ProductDTO> productDTOList = new ArrayList<>();
    var accountIds = products.stream().map(Product::getAccountId).collect(Collectors.toList());
    var productIds = products.stream().map(Product::getId).collect(Collectors.toList());
    var accountMap =
        accountRepository.findAllByIdIn(accountIds).stream()
            .collect(Collectors.toMap(Account::getId, Function.identity()));

    var images = imageRepository.findAllByProductIdIn(productIds);
    var categoryMap =
        IterableUtils.toList(productCategoryRepository.findAll()).stream()
            .collect(Collectors.toMap(ProductCategory::getId, ProductCategory::getName));

    Map<Long, List<String>> imageMap = new HashMap<>();
    products.forEach(
        x -> {
          List<String> imageList = new ArrayList<>();
          images.forEach(
              i -> {
                if (i.getProductId() == (x.getId())) imageList.add(i.getContent());
              });
          imageMap.put(x.getId(), imageList);
        });

    products.forEach(
        x -> {
          ProductDTO productDTO =
              new ProductDTO(
                  x,
                  accountMap.get(x.getId()),
                  imageMap.get(x.getId()),
                  categoryMap.get(x.getCategoryId()));
          productDTOList.add(productDTO);
        });

    return new PageImpl<>(productDTOList, pageable, all.getTotalElements());
  }

  @Override
  public void updateForAdmin(ProductUpdateDTO dto, Long id) throws ApplicationException {
    Optional<Product> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    if (StringUtils.isBlank(dto.getName())
        || Objects.isNull(dto.getCategoryId())
        || StringUtils.isBlank(dto.getDescription())) {
      throw new ApplicationException("", "Không được để trống các trường bắt buộc !");
    }
    var product = byId.get();
    product.setName(dto.getName());
    product.setCategoryId(dto.getCategoryId());
    product.setDescription(dto.getDescription());
    product.setStatus(dto.getStatus());
    repository.save(product);
  }
}
