package com.saleweb.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Image;
import com.saleweb.domains.Post;
import com.saleweb.domains.PostCategory;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.post.PostCreateDTO;
import com.saleweb.models.post.PostDTO;
import com.saleweb.models.post.PostUpdateDTO;
import com.saleweb.repositories.ImageRepository;
import com.saleweb.repositories.PostCategoryRepository;
import com.saleweb.repositories.PostRepository;
import com.saleweb.services.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostServiceImpl implements PostService {

  private final PostRepository repository;
  private final PostCategoryRepository postCategoryRepository;
  private final ObjectMapper mapper;
  private final ImageRepository imageRepository;

  @Override
  @Transactional
  public void create(PostCreateDTO dto) throws ApplicationException {
    // check name
    if (Objects.isNull(dto.getCategoryId())) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_NAME_NULL,
          ExceptionUtils.messages.get(ExceptionUtils.E_POST_NAME_NULL));
    }
    // check loại
    var categoryOptional = postCategoryRepository.findById(dto.getCategoryId());
    if (categoryOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST),
              dto.getCategoryId()));
    }
    if (dto.getImages().isEmpty() || Objects.isNull(dto.getImages())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Ảnh "));
    }

    if (StringUtils.isBlank(dto.getContent())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Nội dung "));
    }
    if (StringUtils.isBlank(dto.getTitle())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tiêu đề "));
    }

    Post post = new Post(dto);
    repository.save(post);
    List<Image> images = new ArrayList<>();
    dto.getImages()
        .forEach(
            x -> {
              images.add(Image.builder().postId(post.getId()).content(x).build());
            });
    imageRepository.saveAll(images);
  }

  @Override
  @Transactional
  public void update(PostUpdateDTO dto, Long id) throws ApplicationException {
    Optional<Post> postOptional = repository.findById(id);
    if (postOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    if (Objects.isNull(dto.getCategoryId())) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_NAME_NULL,
          ExceptionUtils.messages.get(ExceptionUtils.E_POST_NAME_NULL));
    }
    // check loại
    var categoryOptional = postCategoryRepository.findById(dto.getCategoryId());
    if (categoryOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST),
              dto.getCategoryId()));
    }
    if (dto.getImages().isEmpty() || Objects.isNull(dto.getImages())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Ảnh "));
    }
    if (StringUtils.isBlank(dto.getContent())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Nội dung "));
    }

    if (StringUtils.isBlank(dto.getTitle())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tiêu đề "));
    }
    var post = postOptional.get();
    change(post, dto);
    repository.save(post);
    imageRepository.deleteAllByPostId(post.getId());
    List<Image> images = new ArrayList<>();

    dto.getImages()
        .forEach(
            i -> {
              images.add(Image.builder().postId(post.getId()).content(i).build());
            });
    imageRepository.saveAll(images);
  }

  private void change(Post post, PostUpdateDTO dto) {
    post.setTitle(dto.getTitle());
    post.setCategoryId(dto.getCategoryId());
    post.setContent(dto.getContent());
    post.setIsHot(dto.getIsHot());
    post.setStatus(dto.getStatus());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    Optional<Post> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    repository.delete(byId.get());
  }

  @Override
  public PostDTO findById(Long id) throws ApplicationException, JsonProcessingException {

    return null;
  }

  @Override
  public Page<PostDTO> getAll(SearchDTO dto, Pageable pageable) throws JsonProcessingException {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Post> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getTitle())) {
            predicates.add(criteriaBuilder.like(root.get("title"), "%" + dto.getTitle() + "%"));
          }
          if (!Objects.isNull(dto.getCategoryId())) {
            predicates.add(criteriaBuilder.equal(root.get("categoryId"), dto.getCategoryId()));
          }
          if (!Objects.isNull(dto.getIsHot())) {
            predicates.add(criteriaBuilder.equal(root.get("isHot"), dto.getIsHot()));
          }
          if (!Objects.isNull(dto.getId())) {
            predicates.add(criteriaBuilder.equal(root.get("id"), dto.getId()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Post> all = repository.findAll(specification, pageable);
    var list = all.getContent();

    var postDTOs = list.stream().map(PostDTO::new).collect(Collectors.toList());

    var categoryMap =
        IterableUtils.toList(postCategoryRepository.findAll()).stream()
            .collect(Collectors.toMap(PostCategory::getId, PostCategory::getName));

    var postIds = list.stream().map(Post::getId).collect(Collectors.toList());
    var images = imageRepository.findAllByPostIdIn(postIds);

    postDTOs.forEach(
        i -> {
          var imageList =
              images.stream()
                  .filter(x -> x.getPostId() == i.getId())
                  .collect(Collectors.toList())
                  .stream()
                  .map(Image::getContent)
                  .collect(Collectors.toList());
          i.setImages(imageList);
          i.setCategoryName(categoryMap.get(i.getCategoryId()));
        });

    return new PageImpl<>(postDTOs, pageable, all.getTotalElements());
  }
}
