package com.saleweb.services.impl;

import com.saleweb.domains.Cart;
import com.saleweb.domains.Image;
import com.saleweb.domains.Product;
import com.saleweb.domains.ProductCategory;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.models.product.ProductDTO;
import com.saleweb.repositories.*;
import com.saleweb.services.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
  private final CartRepository cartRepository;
  private final ProductRepository productRepository;
  private final ImageRepository imageRepository;
  private final ProductCategoryRepository categoryRepository;
  private final AccountRepository accountRepository;

  @Override
  public void addToCart(Long accountId, Long productId) {
    var cartOpt = cartRepository.findByAccountIdAndProductId(accountId, productId);
    if (cartOpt.isEmpty()) cartRepository.save(new Cart(productId, accountId));
    else {
      var cart = cartOpt.get();
      cart.setQuantity(cart.getQuantity() + 1);
      cartRepository.save(cart);
    }
  }

  @Override
  public void addToCart(Long accountId, Long productId, int quantity)  {
    var cartOpt = cartRepository.findByAccountIdAndProductId(accountId, productId);
    if (cartOpt.isEmpty()) cartRepository.save(new Cart(productId, accountId,quantity));
    else {
      var cart = cartOpt.get();
      cart.setQuantity(cart.getQuantity() + quantity);
      cartRepository.save(cart);
    }
  }

  @Override
  public void removeToCart(List<Long> ids) {
    cartRepository.deleteAllById(ids);
  }

  @Override
  @Transactional
  public void updateCart(List<ProductDTO> list)  {
    var ids=list.stream().map(ProductDTO::getCartId).collect(Collectors.toList());
    var carts=cartRepository.findAllByIdIn(ids);
    var quantityMap=list.stream().collect(Collectors.toMap(ProductDTO::getCartId,ProductDTO::getQuantityCart));
    carts.forEach(x->x.setQuantity(quantityMap.get(x.getId())));
    cartRepository.saveAll(carts);
  }

  @Override
  public List<ProductDTO> getCart(Long accountId) {
    var productOfAccount = cartRepository.findByAccountId(accountId);
    var productIds = productOfAccount.stream().map(Cart::getProductId).collect(Collectors.toList());
    var products = productRepository.findByIdIn(productIds);
    var images =
        imageRepository.findAllByProductIdIn(productIds).stream()
            .sorted(Comparator.comparing(Image::getId))
            .collect(Collectors.toList());
    Map<Long, List<String>> mapImage = new HashMap<>();
    for (Long productId : productIds) {
      List<String> imageList = new ArrayList<>();
      for (Image image : images) {
        if (image.getProductId().longValue() == image.getProductId().longValue()) {
          imageList.add(image.getContent());
        }
      }

      mapImage.put(productId, imageList);
    }
    var categories =
        IterableUtils.toList(categoryRepository.findAll()).stream()
            .collect(Collectors.toMap(ProductCategory::getId, ProductCategory::getName));
    List<ProductDTO> productDTOS = new ArrayList<>();
    for (Product product : products) {
      ProductDTO productDTO =
          new ProductDTO(
              product,
              null,
              mapImage.get(product.getId()),
              categories.get(product.getCategoryId()));
      productDTOS.add(productDTO);
    }

    var cartQuantityMap =
        productOfAccount.stream()
            .collect(Collectors.toMap(Cart::getProductId, Function.identity()));
    productDTOS.forEach(
        x -> {
          x.setQuantityCart(cartQuantityMap.get(x.getId()).getQuantity());
          x.setDateAddCart(cartQuantityMap.get(x.getId()).getCreatedDate());
          x.setCartId(cartQuantityMap.get(x.getId()).getId());
        });
    return productDTOS.stream()
        .sorted(Comparator.comparing(ProductDTO::getDateAddCart).reversed())
        .collect(Collectors.toList());
  }
}
