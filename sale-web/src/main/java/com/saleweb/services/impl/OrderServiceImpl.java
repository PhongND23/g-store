package com.saleweb.services.impl;

import com.saleweb.common.SearchDTO;
import com.saleweb.domains.*;
import com.saleweb.models.order.OrderCreateDTO;
import com.saleweb.models.order.OrderDTO;
import com.saleweb.models.order.OrderDetailDTO;
import com.saleweb.models.order.SellerDTO;
import com.saleweb.models.product.ProductDTO;
import com.saleweb.repositories.*;
import com.saleweb.services.CartService;
import com.saleweb.services.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;
  private final CartService cartService;
  private final CartRepository cartRepository;
  private final PaymentsRepository paymentsRepository;
  private final OrdersStatusRepository ordersStatusRepository;
  private final AccountRepository accountRepository;
  private final ProductRepository productRepository;
  private final OrderDetailRepository orderDetailRepository;
  private final UsersRepository usersRepository;

  @Override
  @Transactional
  public void payment(OrderCreateDTO dto) {
    var cartIds =
        dto.getProducts().stream().map(ProductDTO::getCartId).collect(Collectors.toList());
    cartRepository.deleteAllById(cartIds);

    Set<Long> sellerIdSet =
        dto.getProducts().stream().map(ProductDTO::getAccountId).collect(Collectors.toSet());
    Map<Long, List<ProductDTO>> map = new HashMap<>();
    for (Long item : sellerIdSet) {
      var list =
          dto.getProducts().stream()
              .filter(x -> x.getAccountId().longValue() == item.longValue())
              .collect(Collectors.toList());
      map.put(item, list);
    }

    for (var item : map.keySet()) {
      Order order = new Order(dto);
      order.setSellerId(item);
      orderRepository.save(order);

      var orderId = order.getId();
      List<OrderDetail> orderDetails = new ArrayList<>();
      map.get(item)
          .forEach(
              x -> {
                orderDetails.add(new OrderDetail(orderId, x));
              });
      orderDetailRepository.saveAll(orderDetails);
    }
  }

  @Override
  public OrderDTO getOrder(Long id) {

    return null;
  }

  @Override
  public List<OrderDTO> getAll(SearchDTO dto) {
    Specification<Order> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (ObjectUtils.isNotEmpty(dto.getCustomerId())) {
            predicates.add(criteriaBuilder.equal(root.get("customerId"), dto.getCustomerId()));
          }
          if (ObjectUtils.isNotEmpty(dto.getSellerId())) {
            predicates.add(criteriaBuilder.equal(root.get("sellerId"), dto.getSellerId()));
          }
          if (ObjectUtils.isNotEmpty(dto.getOrderStatusId())) {
            predicates.add(
                criteriaBuilder.equal(root.get("orderStatusId"), dto.getOrderStatusId()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    var orders = orderRepository.findAll(specification);
    var paymentMap =
        IterableUtils.toList(paymentsRepository.findAll()).stream()
            .collect(Collectors.toMap(Payments::getId, Payments::getName));
    var statusMap =
        IterableUtils.toList(ordersStatusRepository.findAll()).stream()
            .collect(Collectors.toMap(OrderStatus::getId, OrderStatus::getName));
    Map<Long, List<OrderDetailDTO>> orderMap = new HashMap<>();
    var ids = orders.stream().map(Order::getId).collect(Collectors.toList());
    var orderDetails = orderDetailRepository.getAllByOrderIdIn(ids);

    orders.forEach(
        x -> {
          var details =
              orderDetails.stream()
                  .filter(d -> d.getOrderId().longValue() == x.getId().longValue())
                  .collect(Collectors.toList());
          orderMap.put(x.getId(), details);
        });

    List<OrderDTO> result = new ArrayList<>();

    orders.forEach(
        x -> {
          OrderDTO orderDTO =
              new OrderDTO(
                  x,
                  statusMap.get(x.getOrderStatusId()),
                  paymentMap.get(x.getPaymentId()),
                  orderMap.get(x.getId()));
          result.add(orderDTO);
        });
    return result.stream()
        .sorted(Comparator.comparing(OrderDTO::getCreatedDate))
        .collect(Collectors.toList());
  }

  @Override
  public Page<OrderDTO> getAllByStatus(Pageable pageable, Long id) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);

    return null;
  }

  @Override
  public void changeStatus(Long id, Long statusId) {
    var orderOptional = orderRepository.findById(id);
    if (orderOptional.isPresent()) {
      orderOptional.get().setOrderStatusId(statusId);
      orderRepository.save(orderOptional.get());
    }
  }

  @Override
  public void buyNow(Long accountId, Long productId, Integer quantity, Long paymentId) {}

  @Override
  public Page<OrderDTO> ordersPage(SearchDTO dto, Pageable pageable) {
    Specification<Order> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (ObjectUtils.isNotEmpty(dto.getCustomerId())) {
            predicates.add(criteriaBuilder.equal(root.get("customerId"), dto.getCustomerId()));
          }
          if (ObjectUtils.isNotEmpty(dto.getSellerId())) {
            predicates.add(criteriaBuilder.equal(root.get("sellerId"), dto.getSellerId()));
          }
          if (ObjectUtils.isNotEmpty(dto.getOrderStatusId())) {
            predicates.add(
                criteriaBuilder.equal(root.get("orderStatusId"), dto.getOrderStatusId()));
          }
          if (ObjectUtils.isNotEmpty(dto.getId())) {
            predicates.add(criteriaBuilder.equal(root.get("id"), dto.getId()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    var page = orderRepository.findAll(specification, pageable);
    var orders = page.getContent();
    var paymentMap =
        IterableUtils.toList(paymentsRepository.findAll()).stream()
            .collect(Collectors.toMap(Payments::getId, Payments::getName));
    var statusMap =
        IterableUtils.toList(ordersStatusRepository.findAll()).stream()
            .collect(Collectors.toMap(OrderStatus::getId, OrderStatus::getName));
    Map<Long, List<OrderDetailDTO>> orderMap = new HashMap<>();
    var ids = orders.stream().map(Order::getId).collect(Collectors.toList());
    var orderDetails = orderDetailRepository.getAllByOrderIdIn(ids);

    orders.forEach(
        x -> {
          var details =
              orderDetails.stream()
                  .filter(d -> d.getOrderId().longValue() == x.getId().longValue())
                  .collect(Collectors.toList());
          orderMap.put(x.getId(), details);
        });

    List<OrderDTO> result = new ArrayList<>();

    orders.forEach(
        x -> {
          OrderDTO orderDTO =
              new OrderDTO(
                  x,
                  statusMap.get(x.getOrderStatusId()),
                  paymentMap.get(x.getPaymentId()),
                  orderMap.get(x.getId()));
          result.add(orderDTO);
        });

    var accountIds = orders.stream().map(Order::getSellerId).collect(Collectors.toList());
    var accounts = accountRepository.findAllByIdIn(accountIds);

    var userIds = accounts.stream().map(Account::getUserId).collect(Collectors.toList());
    var users = usersRepository.findAllByIdIn(userIds);
    Map<Long, User> userMap = new HashMap<>();
    accounts.forEach(
        x -> {
          users.forEach(
              u -> {
                if (x.getUserId().longValue() == u.getId().longValue()) {
                  userMap.put(x.getId(), u);
                }
              });
        });

    for (OrderDTO item : result) {
      item.setSellerDTO(new SellerDTO(userMap.get(item.getSellerId())));
    }

    var sortedResult =
        result.stream()
            .sorted(Comparator.comparing(OrderDTO::getCreatedDate).reversed())
            .collect(Collectors.toList());
    return new PageImpl<>(sortedResult, pageable, page.getTotalElements());
  }

  @Override
  public void cancelOrder(Long orderId, Long accountId) {}

  @Override
  public List<OrderDTO> getOrdersBySeller(Long accountId) {
    return null;
  }
}
