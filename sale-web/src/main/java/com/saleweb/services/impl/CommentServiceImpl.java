package com.saleweb.services.impl;

import com.saleweb.domains.Account;
import com.saleweb.domains.Comment;
import com.saleweb.domains.Product;
import com.saleweb.models.comment.CommentDTO;
import com.saleweb.repositories.AccountRepository;
import com.saleweb.repositories.CommentRepository;
import com.saleweb.repositories.ProductRepository;
import com.saleweb.services.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommentServiceImpl implements CommentService {
  private final CommentRepository repository;
  private final AccountRepository accountRepository;
  private final ProductRepository productRepository;

  @Override
  public void create(Comment comment) {
    comment.setPostedDate(LocalDate.now());
    comment.setStatus(true);
    repository.save(comment);
  }

  @Override
  public void update(Long id, Boolean status) {
    var cmtOpt = repository.findById(id);
    if (cmtOpt.isPresent()) {
      cmtOpt.get().setStatus(status);
      repository.save(cmtOpt.get());
    }
  }

  @Override
  public Page<CommentDTO> getAll(Long productId, Long postId,Long accountId, Boolean status, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Comment> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (ObjectUtils.isNotEmpty(productId)) {
            predicates.add(criteriaBuilder.equal(root.get("productId"), productId));
          }
          if (ObjectUtils.isNotEmpty(postId)) {
            predicates.add(criteriaBuilder.equal(root.get("postId"), postId));
          }
            if (ObjectUtils.isNotEmpty(accountId)) {
                predicates.add(criteriaBuilder.equal(root.get("accountId"), accountId));
            }

          if (ObjectUtils.isNotEmpty(status)) {
            predicates.add(criteriaBuilder.equal(root.get("status"), status));
          }

          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Comment> page = repository.findAll(specification, pageable);
    var accountIds =
        page.getContent().stream().map(Comment::getAccountId).collect(Collectors.toList());
    var productIds=page.getContent().stream().map(Comment::getProductId).collect(Collectors.toList());
    var productMap=productRepository.findByIdIn(productIds).stream().collect(Collectors.toMap(Product::getId,Function.identity()));
    var accountMap =
        accountRepository.findAllByIdIn(accountIds).stream()
            .collect(Collectors.toMap(Account::getId, Function.identity()));
    var list=page.getContent().stream().map(CommentDTO::new).collect(Collectors.toList());
    list.forEach(x->{
        x.setAccount(accountMap.get(x.getAccountId()));
        x.setProduct(productMap.get(x.getProductId()));
    });
    return new PageImpl<>(list,pageable,page.getTotalElements());
  }

    @Override
    public Boolean allowComment(Long accountId, Long productId) {
        return repository.countOrder(accountId,productId)>0;
    }
}
