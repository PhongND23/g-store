package com.saleweb.services.impl;

import com.saleweb.common.Constants;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Account;
import com.saleweb.domains.Role;
import com.saleweb.domains.User;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.account.*;
import com.saleweb.repositories.*;
import com.saleweb.services.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

  private final AccountRepository repository;
  private final RoleRepository roleRepository;
  private final UsersRepository usersRepository;
  private final AccountRepository accountRepository;

  @Override
  @Transactional
  public void create(AccountCreateDTO dto) throws ApplicationException {
    // check userName
    if (StringUtils.isBlank(dto.getUserName())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tên đăng nhập "));
    }
    if (repository.existsByUserName(dto.getUserName())) {
      throw new ApplicationException(
          ExceptionUtils.DUPLICATE_USERNAME,
          ExceptionUtils.messages.get(ExceptionUtils.DUPLICATE_USERNAME));
    }
    if (StringUtils.isBlank(dto.getPassword())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Password "));
    }
    if (Objects.isNull(dto.getRoleId())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Nhóm quyền "));
    }
    // check roleId
    if (!roleRepository.existsById(dto.getRoleId())) {
      throw new ApplicationException(
          ExceptionUtils.E_ROLE_ID_NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.E_ROLE_ID_NOT_EXIST), dto.getRoleId()));
    }
    // check userID
    if (Objects.isNull(dto.getUserDTO())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Thông tin cá nhân "));
    }

    if (StringUtils.isBlank(dto.getUserDTO().getFullName())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Họ tên "));
    }
    if (StringUtils.isBlank(dto.getUserDTO().getAddress())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Địa chỉ "));
    }

    if (StringUtils.isBlank(dto.getUserDTO().getPhoneNumber())
        || Objects.isNull(dto.getUserDTO().getPhoneNumber())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Số điện thoại "));
    }
      User user = new User(dto.getUserDTO());
    usersRepository.save(user);
    Account account = new Account(dto);
    account.setUserId(user.getId());
    repository.save(account);
  }

  @Override
  @Transactional
  public void update(AccountCreateDTO dto, Long id) throws ApplicationException {
    Optional<Account> accountOpt = repository.findById(id);
    if (accountOpt.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST), id));
    }
    if (StringUtils.isBlank(dto.getPassword())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Password "));
    }
    if (dto.getRoleId() == null) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Quyền "));
    }
    if (!roleRepository.existsById(dto.getRoleId())) {
      throw new ApplicationException(
          ExceptionUtils.E_ROLE_ID_NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.E_ROLE_ID_NOT_EXIST), dto.getRoleId()));
    }

    var account=accountOpt.get();
    var userOpt=usersRepository.findById(account.getUserId());
    if (userOpt.isEmpty()) {
      throw new ApplicationException(
              ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST), id));
    }

    account.setPassword(dto.getPassword());
    account.setRoleId(dto.getRoleId());
    account.setStatus(dto.getStatus());
    account.setAvatar(dto.getAvatar());
    repository.save(account);

    var user=userOpt.get();
    user.setFullName(dto.getUserDTO().getFullName());
    user.setDateOfBirth(dto.getUserDTO().getDateOfBirth());
    user.setEmail(dto.getUserDTO().getEmail());
    user.setAddress(dto.getUserDTO().getAddress());
    user.setGender(dto.getUserDTO().getGender());
    user.setPhoneNumber(dto.getUserDTO().getPhoneNumber());
    usersRepository.save(user);
  }

  @Override
  @Transactional
  public void delete(Long id) throws ApplicationException {
    var accountOpt=accountRepository.findById(id);
    if (accountOpt.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST), id));
    }
//    var products = productRepository.findByAccountId(id);
//    if (CollectionUtils.isNotEmpty(products)) {
//      throw new ApplicationException(
//          "", "Không thể xóa tài khoản do tài khoản đang đăng bán sản phẩm");
//    }
    repository.deleteById(id);
    usersRepository.deleteById(accountOpt.get().getUserId());
  }

  @Override
  public AccountDTO findById(Long id) throws ApplicationException {
    return null;
  }

  @Override
  public Page<AccountDTO> getAll(SearchDTO dto, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Account> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getUserName())) {
            predicates.add(
                criteriaBuilder.like(root.get("userName"), "%" + dto.getUserName() + "%"));
          }

          if (ObjectUtils.isNotEmpty(dto.getRoleId())) {
            predicates.add(
                    criteriaBuilder.equal(root.get("roleId"), dto.getRoleId()));
          }




          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Account> all = repository.findAll(specification, pageable);
    List<AccountDTO> list = new ArrayList<>();
    var roleMap =
        IterableUtils.toList(roleRepository.findAll()).stream()
            .collect(Collectors.toMap(Role::getId, Role::getName));
    var ids = all.getContent().stream().map(Account::getUserId).collect(Collectors.toList());

    var userMap =
        usersRepository.findAllByIdIn(ids).stream()
            .collect(Collectors.toMap(User::getId, Function.identity()));

    all.forEach(
        x -> {
          list.add(new AccountDTO(x, userMap.get(x.getUserId()), roleMap.get(x.getRoleId())));
        });

    return new PageImpl<>(list, pageable, all.getTotalElements());
  }

  @Override
  public AccountDTO Login(Login login) throws ApplicationException {
      var accountOpt=accountRepository.findByUserNameAndPassword(login.getUserName(),login.getPassword());
      if(accountOpt.isEmpty())
      {
        throw new ApplicationException("","Tên đăng nhập hoặc mật khẩu không khớp");
      }
      var account=accountOpt.get();
      if(BooleanUtils.isFalse(login.getLoginSystem()) && account.getRoleId()==Constants.ADMIN_ROLE_ID)
      {
        throw new ApplicationException("","Tên đăng nhập hoặc mật khẩu không khớp");
      }
      var userOpt=usersRepository.findById(account.getUserId());
      return new AccountDTO(account,userOpt.get(),StringUtils.EMPTY);
  }

  @Override
  public AccountQueryDTO LoginAdmin(Login login) throws ApplicationException {
    //    var accountOptional =
    //        accountRepository.getByUsernameAndPassword(login.getUserName(), login.getPassword());
    //    if (accountOptional == null) {
    //      throw new ApplicationException(
    //          ExceptionUtils.ACCOUNT_NOT_EXIST,
    //          ExceptionUtils.messages.get(ExceptionUtils.ACCOUNT_NOT_EXIST));
    //    }
    //    if (accountOptional.getRoleId() != 1) {
    //      throw new ApplicationException(
    //          ExceptionUtils.AUTHORIZE, ExceptionUtils.messages.get(ExceptionUtils.AUTHORIZE));
    //    }
    //    if (BooleanUtils.isFalse(accountOptional.getStatus())) {
    //      throw new ApplicationException(
    //          ExceptionUtils.LOCK_ACCOUNT,
    // ExceptionUtils.messages.get(ExceptionUtils.LOCK_ACCOUNT));
    //    }
    //    return accountOptional;
    return null;
  }

  @Override
  public void changePass(Long id, String pass) {
    var account = accountRepository.findById(id);
    if (account.isPresent()) {
      account.get().setPassword(pass);
      accountRepository.save(account.get());
    }
  }
}
