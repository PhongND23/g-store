package com.saleweb.services;

import com.saleweb.domains.Role;
import com.saleweb.models.category.DropDownDTO;

import java.util.List;

public interface RoleService extends BaseService<Role, Role, Role> {

    List<DropDownDTO> getDrop();
}
