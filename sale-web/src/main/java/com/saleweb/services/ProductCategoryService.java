package com.saleweb.services;

import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.category.ProductCategoryCreateDTO;
import com.saleweb.models.category.ProductCategoryUpdateDTO;
import com.saleweb.domains.ProductCategory;

import java.util.List;

public interface ProductCategoryService
    extends BaseService<ProductCategoryCreateDTO, ProductCategoryUpdateDTO, ProductCategory> {
    List<DropDownDTO> getDropDown();
}
