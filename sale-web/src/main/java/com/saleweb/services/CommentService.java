package com.saleweb.services;

import com.saleweb.domains.Comment;
import com.saleweb.models.comment.CommentDTO;
import com.saleweb.models.user.UserCreateDTO;
import com.saleweb.models.user.UserDTO;
import com.saleweb.models.user.UserUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentService {
    void create(Comment comment);
    void update(Long id,Boolean status);
    Page<CommentDTO> getAll(Long productId, Long postId,Long accountId, Boolean status, Pageable pageable);
    Boolean allowComment(Long accountId,Long productId);
}
