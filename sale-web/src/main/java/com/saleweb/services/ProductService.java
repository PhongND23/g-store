package com.saleweb.services;

import com.saleweb.domains.Product;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.product.ProductCreateDTO;
import com.saleweb.models.product.ProductDTO;
import com.saleweb.models.product.ProductUpdateDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService
    extends BaseService<ProductCreateDTO, ProductUpdateDTO, ProductDTO> {
        void updateForAdmin(ProductUpdateDTO dto,Long id) throws ApplicationException;
}
