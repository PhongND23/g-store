package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import com.saleweb.models.product.ProductCreateDTO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "product")
public class Product {
  @Id
  @SequenceGenerator(
      name = "product_sequence_id",
      sequenceName = "product_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "category_id")
  private Long categoryId;

  @Column(name = "price")
  private Double price;

  @Column(name = "promotion_price")
  private Double promotionPrice;

  @Column(name = "quantity")
  private Integer quantity;

  @Column(name = "description")
  private String description;

  @Column(name = "short_description")
  private String shortDescription;

  @Column(name = "status")
  private Boolean status;

  @Column(name = "unit")
  private String unit;

  @Column(name = "account_id")
  private Long accountId;

  @Column(name = "created_date")
  private LocalDateTime createdDate;

  @Column(name = "updated_date")
  private LocalDateTime updatedDate;

  public Product(ProductCreateDTO p) {

    this.name = p.getName();
    this.unit = p.getUnit();
    this.categoryId = p.getCategoryId();
    this.accountId = p.getAccountId();
    this.price = p.getPrice();
    this.promotionPrice = p.getPromotionPrice();
    this.quantity = p.getQuantity();
    this.description=p.getDescription();
    this.shortDescription=p.getShortDescription();
    this.createdDate=LocalDateTime.now();
    this.status=Boolean.TRUE;
  }
}
