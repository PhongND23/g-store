package com.saleweb.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "comment")
public class Comment {
  @Id
  @SequenceGenerator(
      name = "comment_sequence_id",
      sequenceName = "comment_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "product_id")
  private Long productId;

  @Column(name = "post_id")
  private Long postId;

  @Column(name = "account_id")
  private Long accountId;

  @Column(name = "content")
  private String content;

  @Column(name = "status")
  private Boolean status;

  @Column(name = "posted_date")
  private LocalDate postedDate;
}
