package com.saleweb.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "image")
public class Image {
  @Id
  @SequenceGenerator(
      name = "image_sequence_id",
      sequenceName = "image_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "image_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "product_id")
  private Long productId;

  @Column(name = "post_id")
  private Long postId;

  @Column(name = "content")
  private String content;
}
