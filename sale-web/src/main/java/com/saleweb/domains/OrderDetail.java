package com.saleweb.domains;

import com.saleweb.models.product.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "order_detail")
public class OrderDetail {
  @Id
  @SequenceGenerator(
      name = "order_detail_sequence_id",
      sequenceName = "order_detail_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_detail_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "order_id")
  private Long orderId;

  @Column(name = "product_id")
  private Long productId;

  @Column(name = "current_price")
  private Double currentPrice;

  @Column(name = "quantity")
  private Integer quantity;


  public OrderDetail(Long orderId, ProductDTO dto) {
    this.orderId = orderId;
    this.productId = dto.getId();
    this.currentPrice =
        ObjectUtils.isEmpty(dto.getPromotionPrice()) ? dto.getPrice() : dto.getPromotionPrice();
    this.quantity = dto.getQuantityCart();
  }
}
