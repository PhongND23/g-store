package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "cart")
public class Cart {
  @Id
  @SequenceGenerator(
      name = "cart_sequence_id",
      sequenceName = "cart_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "product_id")
  private Long productId;

  @Column(name = "account_id")
  private Long accountId;

  @Column(name = "quantity")
  private Integer quantity;

  @Column(name = "created_date")
  private LocalDate createdDate;

  public Cart(Long productId,Long accountId)
  {
    this.productId=productId;
    this.accountId=accountId;
    this.quantity=1;
    this.createdDate= LocalDate.now();
  }
  public Cart(Long productId,Long accountId,int quantity)
  {
    this.productId=productId;
    this.accountId=accountId;
    this.quantity=quantity;
    this.createdDate= LocalDate.now();
  }

}
