package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import com.saleweb.models.account.AccountCreateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "account")
public class Account extends Auditable {
  @Id
  @SequenceGenerator(
      name = "account_sequence_id",
      sequenceName = "account_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "username")
  private String userName;

  @Column(name = "password")
  private String password;

  @Column(name = "role_id")
  private Long roleId;

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "status")
  private Boolean status;

  @Column(name = "avatar")
  private String avatar;

  public Account(AccountCreateDTO dto) {
    this.setUserName(dto.getUserName());
    this.setPassword(dto.getPassword());
    this.setRoleId(dto.getRoleId());
    this.setStatus(dto.getStatus());
    this.setAvatar(dto.getAvatar());
  }
}
