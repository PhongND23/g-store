package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import com.saleweb.models.post.PostCreateDTO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "post")
public class Post extends Auditable {
  @Id
  @SequenceGenerator(
      name = "post_sequence_id",
      sequenceName = "post_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "title")
  private String title;

  @Column(name = "category_id")
  private Long categoryId;

  @Column(name = "content")
  private String content;

  @Column(name = "is_hot")
  private Boolean isHot;

  @Column(name = "view_count")
  private Integer viewCount;

  @Column(name = "like_count")
  private Integer likeCount;

  @Column(name = "status")
  private Boolean status;


  public Post(PostCreateDTO p) {
    this.title = p.getTitle();
    this.categoryId = p.getCategoryId();
    this.content = p.getContent();
    this.isHot = p.getIsHot();
    this.viewCount = 0;
    this.likeCount = 0;
    this.status = Boolean.TRUE ;
    this.setCreatedBy(p.getCreatedBy());
  }
}
