package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import com.saleweb.models.order.OrderCreateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "orders")
public class Order {
  @Id
  @SequenceGenerator(
      name = "order_sequence_id",
      sequenceName = "order_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "customer_id")
  private Long customerId;

  @Column(name = "seller_id")
  private Long sellerId;

  @Column(name = "payment_id")
  private Long paymentId;

  @Column(name = "status_id")
  private Long orderStatusId;

  @Column(name = "ship")
  private Double ship;

  @Column(name = "created_date")
  private LocalDateTime createdDate;

  @Column(name = "address")
  private String address;

  @Column(name = "phone")
  private String phone;

  @Column(name = "full_name")
  private String fullName;

  @Column(name = "note")
  private String note;

  public Order(OrderCreateDTO dto)
  {
    this.customerId=dto.getCustomerId();
    this.paymentId=1L;
    this.address=dto.getAddress();
    this.phone=dto.getPhone();
    this.fullName=dto.getFullName();
    this.note=dto.getNote();
    this.createdDate=LocalDateTime.now();
    this.orderStatusId=1l;
    this.ship=15000d;
  }

}
