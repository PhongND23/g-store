package com.saleweb.controllers;

import com.saleweb.common.MessageDTO;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Comment;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.user.UserUpdateDTO;
import com.saleweb.services.CommentService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/comment")
@RestController
public class CommentController {
  private final CommentService service;


  @CrossOrigin
  @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@RequestBody Comment comment) {
    try {
      service.create(comment);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage(),ex);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @CrossOrigin
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@PathVariable Long id, @RequestParam(value = "status") Boolean status) {
    try {
      service.update(id,status);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage(),ex);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @CrossOrigin
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findAll(
      @RequestParam(required = false) Long productId,
      @RequestParam(required = false) Long postId,
      @RequestParam(required = false) Long accountId,
      @RequestParam(required = false) Boolean status,
      @Parameter(hidden = true) Pageable pageable) {
    try {
      return new ResponseEntity<>(service.getAll(productId,postId,accountId,status, pageable), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/allow",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> allowComment(
          @RequestParam(required = false) Long productId,
          @RequestParam(required = false) Long accountId) {
    try {
      return new ResponseEntity<>(service.allowComment(accountId,productId), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage(),ex);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
