package com.saleweb.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.common.MessageDTO;
import com.saleweb.common.SearchDTO;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.product.ProductCreateDTO;
import com.saleweb.models.product.ProductUpdateDTO;
import com.saleweb.services.ProductService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/product")
@RestController
public class ProductController {
  private final ProductService service;
  private ObjectMapper mapper;

  @CrossOrigin
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> create(@RequestBody ProductCreateDTO dto) {
    try {
      service.create(dto);
      return new ResponseEntity<>(HttpStatus.CREATED);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody ProductUpdateDTO dto) {
    try {
      service.update(dto, id);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @PutMapping(value = "/admin/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> updateAdmin(@PathVariable Long id, @RequestBody ProductUpdateDTO dto) {
    try {
      service.updateForAdmin(dto, id);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> delete(@PathVariable Long id) {
    try {
      service.delete(id);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      return new ResponseEntity<>(
          new MessageDTO(ExceptionUtils.E_INTERNAL_SERVER), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findAll(
      @RequestParam(value = "name",required = false) String name,
      @RequestParam(value = "id",required = false) Long id,
      @RequestParam(value = "categoryId",required = false) Long categoryId,
      @RequestParam(value = "accountId",required = false) Long accountId,
      @RequestParam(value = "fromPrice",required = false) Double fromPrice,
      @RequestParam(value = "toPrice",required = false) Double toPrice,
      @RequestParam(value = "status",required = false) Boolean status,
      @Parameter(hidden = true) Pageable pageable)
      {
    try {
      SearchDTO dto = new SearchDTO();
      dto.setName(name);
      dto.setCategoryId(categoryId);
      dto.setAccountId(accountId);
      dto.setFromPrice(fromPrice);
      dto.setToPrice(toPrice);
      dto.setStatus(status);
      dto.setId(id);
      return new ResponseEntity<>(service.getAll(dto, pageable), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
