package com.saleweb.controllers;

import com.saleweb.common.MessageDTO;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.services.OrderService;
import com.saleweb.services.OrderStatusService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/order-status")
@RestController
public class OrderStatusController {
  private final OrderStatusService service;



  @CrossOrigin
  @GetMapping(value = "/drop",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAll() {
    try {
      return new ResponseEntity<>(service.getDrop(),HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
