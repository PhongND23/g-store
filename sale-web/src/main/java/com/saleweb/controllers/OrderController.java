package com.saleweb.controllers;

import com.saleweb.common.MessageDTO;
import com.saleweb.common.SearchDTO;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.order.OrderCreateDTO;
import com.saleweb.services.OrderService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/order")
@RestController
public class OrderController {
  private final OrderService service;

  @CrossOrigin
  @PostMapping(value = "/payment", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> payment(@RequestBody OrderCreateDTO dto) {
    try {
      service.payment(dto);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getOrder(@PathVariable Long id) {
    try {
      return new ResponseEntity<>(service.getOrder(id), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @PostMapping(value = "customer-order", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAll(@RequestBody SearchDTO dto) {
    try {
      return new ResponseEntity<>(service.getAll(dto), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/status/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAllByStatus(
      @Parameter(hidden = true) Pageable pageable, @PathVariable Long id) {
    try {
      return new ResponseEntity<>(service.getAllByStatus(pageable, id), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "change/{id}/{statusId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> changeStatus(@PathVariable Long id, @PathVariable Long statusId) {
    try {
      service.changeStatus(id, statusId);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(
      value = "buy/{accountId}/{productId}/{quantity}/{paymentId}",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> buyOneProduct(
      @PathVariable Long accountId,
      @PathVariable Long productId,
      @PathVariable int quantity,
      @PathVariable Long paymentId) {
    try {
      service.buyNow(accountId, productId, quantity, paymentId);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (ApplicationException ex) {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @PostMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getOrderOfAccount(
      @RequestBody SearchDTO dto, @Parameter(hidden = true) Pageable pageable) {
    try {
      return new ResponseEntity<>(service.ordersPage(dto, pageable), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "cancel/{orderId}/{accountId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> cancel(@PathVariable Long orderId, @PathVariable Long accountId) {
    try {
      service.cancelOrder(orderId, accountId);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "seller/{accountId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getOrderOfSeller(@PathVariable Long accountId) {
    try {
      return new ResponseEntity<>(service.getOrdersBySeller(accountId), HttpStatus.OK);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
