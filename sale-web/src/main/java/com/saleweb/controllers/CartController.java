package com.saleweb.controllers;

import com.saleweb.common.MessageDTO;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Tag;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.models.product.ProductDTO;
import com.saleweb.services.CartService;
import com.saleweb.services.TagService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/cart")
@RestController
public class CartController {
  private final CartService service;


  @CrossOrigin
  @GetMapping(value = "/add-to-cart/{accountId}/{productId}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> addToCart(@PathVariable Long accountId,@PathVariable Long productId) {
    try {
      service.addToCart(accountId,productId);
      return new ResponseEntity<>(HttpStatus.OK);
    }catch (ApplicationException ex)
    {
      return new ResponseEntity<>(
          new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/add-to-cart/{accountId}/{productId}/{quantity}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> addToCart(@PathVariable Long accountId,@PathVariable Long productId,@PathVariable int quantity) {
    try {
      service.addToCart(accountId,productId,quantity);
      return new ResponseEntity<>(HttpStatus.OK);
    }catch (ApplicationException ex)
    {
      return new ResponseEntity<>(
              new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @PostMapping(value = "/remove-to-cart",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> removeToCart(@RequestBody List<Long> ids) {
    try {
      service.removeToCart(ids);
      return new ResponseEntity<>(HttpStatus.OK);
    }catch (ApplicationException ex)
    {
      return new ResponseEntity<>(
              new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/get-cart/{accountId}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getCart(@PathVariable Long accountId) {
    try {

      return new ResponseEntity<>(service.getCart(accountId),HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @PostMapping(value = "/update-cart",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> updateCart( @RequestBody List<ProductDTO> list) {
    try {
      service.updateCart(list);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
