package com.saleweb.models.user;

import com.saleweb.domains.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserDTO {
  private Long id;

  private String fullName;

  private String address;

  private String email;

  private String phoneNumber;

  private Boolean gender;

  private LocalDate dateOfBirth;

  public UserDTO(User user) {

    this.setId(user.getId());
    this.setFullName(user.getFullName());
    this.setAddress(user.getAddress());
    this.setEmail(user.getEmail());
    this.setPhoneNumber(user.getPhoneNumber());
    this.setGender(user.getGender());
    this.setDateOfBirth(user.getDateOfBirth());
  }
}
