package com.saleweb.models.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.domains.Product;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductCreateDTO {

  private String name;
  @JsonProperty
  private Long categoryId;
  private Long accountId;
  private Double price;
  private Double promotionPrice;
  private int quantity;
  private String description;
  private String shortDescription;
  private String unit;
  private String productStatus;
  private String address;
  private List<String> images;
}
