package com.saleweb.models.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductUpdateDTO {

  private String name;
  private Long categoryId;
  private Double price;
  private Double promotionPrice;
  private int quantity;
  private String description;
  private String shortDescription;
  private String unit;
  private List<String> images;
  @JsonProperty
  private Boolean status;


}
