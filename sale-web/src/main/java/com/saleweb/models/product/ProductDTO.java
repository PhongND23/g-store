package com.saleweb.models.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.domains.Account;
import com.saleweb.domains.Product;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductDTO {
  private Long id;

  private String name;

  private Long categoryId;

  private String categoryName;

  private Double price;

  private Double promotionPrice;

  private Integer quantity;

  private String description;

  private String shortDescription;

  @JsonProperty private Boolean status;

  private String unit;

  private LocalDateTime createdDate;

  private LocalDateTime updatedDate;

  private Long accountId;

  private Account account;

  private List<String> images;

  private Integer quantityCart;
  private LocalDate dateAddCart;
  private Long cartId;

  public ProductDTO(Product product,Account account,List<String> images,String categoryName) {
    this.setId(product.getId());
    this.setName(product.getName());
    this.setCategoryId(product.getCategoryId());
    this.setCategoryName(categoryName);
    this.setPrice(product.getPrice());
    this.setPromotionPrice(product.getPromotionPrice());
    this.setQuantity(product.getQuantity());
    this.setDescription(product.getDescription());
    this.setShortDescription(product.getShortDescription());
    this.setStatus(product.getStatus());
    this.setUnit(product.getUnit());
    this.setCreatedDate(product.getCreatedDate());
    this.setUpdatedDate(product.getUpdatedDate());
    this.setImages(images);
    this.setAccount(account);
    this.setAccountId(product.getAccountId());
  }

}
