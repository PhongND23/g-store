package com.saleweb.models.comment;

import com.saleweb.domains.Account;
import com.saleweb.domains.Comment;
import com.saleweb.domains.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentDTO {
    private Long id;

    private Long productId;

    private Long postId;

    private String content;

    private Boolean status;

    private LocalDate postedDate;

    private Long accountId;

    private Account account;
    private Product product;

    public CommentDTO(Comment comment)
    {
        this.id=comment.getId();
        this.productId=comment.getProductId();
        this.postId=comment.getPostId();
        this.content=comment.getContent();
        this.status=comment.getStatus();
        this.postedDate=comment.getPostedDate();
        this.accountId=comment.getAccountId();
    }
}
