package com.saleweb.models.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.domains.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RoleDTO {

  private Long id;

  private String name;

  @JsonProperty private Boolean status;

  private String description;

  public RoleDTO(Role role) {
    this.setId(role.getId());
    this.setName(role.getName());
    this.setDescription(role.getDescription());
    this.setStatus(role.getStatus());
  }
}
