package com.saleweb.models.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostUpdateDTO {

  private String title;

  private Long categoryId;

  private List<String> images;

  private String sortDescription;

  private String content;

  @JsonProperty private Boolean isHot;

  private Integer viewCount;

  private Integer likeCount;
  @JsonProperty private Boolean status;

  private String tags;
}
