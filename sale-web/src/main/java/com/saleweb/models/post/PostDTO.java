package com.saleweb.models.post;

import com.saleweb.domains.Post;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostDTO {
  private Long id;

  private String title;

  private Long categoryId;

  private String categoryName;

  private List<String> images;

  private String content;

  private Boolean isHot;

  private Integer viewCount;

  private Integer likeCount;

  private Boolean status;

  private LocalDateTime createdDate;

  public PostDTO(Post post) {
    this.setId(post.getId());
    this.setTitle(post.getTitle());
    this.setCategoryId(post.getCategoryId());
    this.setContent(post.getContent());
    this.setIsHot(post.getIsHot());
    this.setViewCount(post.getViewCount());
    this.setLikeCount(post.getLikeCount());
    this.setStatus(post.getStatus());
    this.setCreatedDate(post.getCreatedDate());
  }
}
