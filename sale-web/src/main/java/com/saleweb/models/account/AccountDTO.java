package com.saleweb.models.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.domains.Account;
import com.saleweb.domains.Role;
import com.saleweb.domains.User;
import com.saleweb.models.role.RoleDTO;
import com.saleweb.models.user.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AccountDTO {

  private Long id;

  private String userName;

  private String password;

  private Long roleId;

  private String roleName;
  private String avatar;

  private UserDTO userDTO;

  @JsonProperty private Boolean status;

  public AccountDTO(Account account, User user, String roleName) {
    this.setId(account.getId());
    this.setAvatar(account.getAvatar());
    this.setUserName(account.getUserName());
    this.setPassword(account.getPassword());
    this.setRoleId(account.getRoleId());
    this.setUserDTO(new UserDTO(user));
    this.setStatus(account.getStatus());
    this.setRoleName(roleName);
  }


}
