package com.saleweb.models.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AccountQueryDTO {

  private Long id;

  private Long userId;

  private String userName;

  private String password;

  private Boolean status;

  private Long roleId;

  private String roleName;

  private String fullName;

  private String address;

  private String email;

  private String phoneNumber;

  private LocalDate dateOfBirth;

  @JsonProperty private Boolean gender;
}
