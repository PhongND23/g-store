package com.saleweb.models.account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AccountUpdateDTO {

    private String password;

    private Long roleId;

    private Boolean status;

}
