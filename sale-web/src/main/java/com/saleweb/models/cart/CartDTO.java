package com.saleweb.models.cart;

import com.saleweb.domains.Cart;
import com.saleweb.models.product.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CartDTO {

    private Long id;

    private Long accountId;

    private Long productId;

    private List<ProductDTO> products;

    public CartDTO(Cart cart, List<ProductDTO> products)
    {
        this.setId(cart.getId());
        this.setAccountId(cart.getAccountId());
        this.setProductId(productId);
    }
}
