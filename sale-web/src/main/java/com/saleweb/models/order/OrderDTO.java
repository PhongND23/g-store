package com.saleweb.models.order;

import com.saleweb.domains.Order;
import com.saleweb.domains.OrderDetail;
import com.saleweb.models.cart.ProductCartDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrderDTO {

  private Long id;

  private Long customerId;
  private Long sellerId;

  private String fullName;
  private String address;
  private String phone;
  private Double ship;

  private String orderStatusName;
  private Long orderStatusId;
  private String paymentName;
  private Long paymentId;
  private List<OrderDetailDTO> products;
  private LocalDateTime createdDate;
  private SellerDTO sellerDTO;

  public OrderDTO(
      Order order,
      String orderStatusName,
      String paymentName,
      List<OrderDetailDTO> products) {

    this.setId(order.getId());
    this.setCustomerId(order.getCustomerId());
    this.setSellerId(order.getSellerId());
    this.setFullName(order.getFullName());
    this.setAddress(order.getAddress());
    this.setPhone(order.getPhone());
    this.setShip(order.getShip());
    this.setCreatedDate(order.getCreatedDate());

    this.setOrderStatusName(orderStatusName);
    this.setOrderStatusId(order.getOrderStatusId());
    this.setPaymentName(paymentName);
    this.setPaymentId(order.getPaymentId());
    this.setProducts(products);
  }
}
