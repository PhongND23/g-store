package com.saleweb.models.order;

import com.saleweb.domains.Order;
import com.saleweb.domains.OrderDetail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrderDetailDTO {

  private Long id;

  private Long orderId;
  private Long productId;

  private Double currentPrice;
  private Integer quantity;
  private String image;
  private String categoryName;
  private String productName;


//  public OrderDetailDTO(
//      OrderDetail order,
//      String categoryName,
//      String image) {
//
//    this.setId(order.getId());
//    this.setOrderId(order.getOrderId());
//    this.setProductId(order.getProductId());
//    this.setCurrentPrice(order.getCurrentPrice());
//    this.setQuantity(order.getQuantity());
//    this.setImage(image);
//    this.setCategoryName(categoryName);
//  }
}
