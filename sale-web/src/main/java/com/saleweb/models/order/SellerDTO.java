package com.saleweb.models.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.domains.Order;
import com.saleweb.domains.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SellerDTO {

  private String fullName;

  private String address;

  private String email;

  private String phoneNumber;

  @JsonProperty
  private Boolean gender;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate dateOfBirth;

  public SellerDTO(User user) {

    this.setFullName(user.getFullName());
    this.setAddress(user.getAddress());
    this.setEmail(user.getEmail());
    this.setPhoneNumber(user.getPhoneNumber());
    this.setGender(user.getGender());
    this.setDateOfBirth(user.getDateOfBirth());

  }
}
