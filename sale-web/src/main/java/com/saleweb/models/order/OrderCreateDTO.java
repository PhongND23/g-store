package com.saleweb.models.order;

import com.saleweb.models.product.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrderCreateDTO {

  private Long id;

  private Long customerId;

  private Long paymentId;

  private String fullName;
  private String address;
  private String phone;
  private String note;

  private List<ProductDTO> products;
}
