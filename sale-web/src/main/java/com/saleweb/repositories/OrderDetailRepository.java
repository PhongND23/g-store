package com.saleweb.repositories;

import com.saleweb.domains.OrderDetail;
import com.saleweb.models.order.OrderDetailDTO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OrderDetailRepository
    extends PagingAndSortingRepository<OrderDetail, Long>, JpaSpecificationExecutor<OrderDetail> {
  List<OrderDetail> findAllByOrderIdIn(List<Long> ids);

  @Query(
      value =
          "select new com.saleweb.models.order.OrderDetailDTO( od.id,od.orderId,od.productId,od.currentPrice,od.quantity,i.content,pc.name,p.name)\n"
              + "from OrderDetail od\n"
              + "left join Product p on od.productId=p.id\n"
              + "left join ProductCategory pc on p.categoryId=pc.id\n"
              + "left join Image i on p.id = i.productId\n"
              + "where od.orderId in(?1)")
  List<OrderDetailDTO> getAllByOrderIdIn(List<Long> ids);
}
