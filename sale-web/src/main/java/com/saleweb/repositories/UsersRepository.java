package com.saleweb.repositories;

import com.saleweb.domains.Role;
import com.saleweb.domains.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UsersRepository
    extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {

    List<User> findAllByIdIn(List<Long> ids);
}
