package com.saleweb.repositories;

import com.saleweb.domains.Post;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PostRepository
    extends PagingAndSortingRepository<Post, Long>, JpaSpecificationExecutor<Post> {
    List<Post> findByCategoryId(Long id);
}
