package com.saleweb.repositories;

import com.saleweb.domains.Image;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ImageRepository
    extends PagingAndSortingRepository<Image, Long>, JpaSpecificationExecutor<Image> {

    List<Image> findAllByProductIdIn(List<Long> ids);
    List<Image> findAllByPostIdIn(List<Long> ids);

    void deleteAllByProductId(Long id);
    void deleteAllByPostId(Long id);
}
