package com.saleweb.repositories;

import com.saleweb.domains.Product;
import com.saleweb.models.product.ProductDTO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository
    extends PagingAndSortingRepository<Product, Long>, JpaSpecificationExecutor<Product> {


  List<Product> findByIdIn(List<Long> ids);

  List<Product> findByCategoryId(Long id);
  List<Product> findByAccountId(Long id);

}
