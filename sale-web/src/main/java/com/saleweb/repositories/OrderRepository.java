package com.saleweb.repositories;

import com.saleweb.domains.Order;
import com.saleweb.models.order.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository
    extends PagingAndSortingRepository<Order, Long>, JpaSpecificationExecutor<Order> {

}
