package com.saleweb.repositories;

import com.saleweb.domains.Comment;
import com.saleweb.domains.User;
import com.saleweb.models.comment.CommentDTO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CommentRepository
    extends PagingAndSortingRepository<Comment, Long>, JpaSpecificationExecutor<Comment> {

    @Query(value = "select count(o.id) from  Order o join OrderDetail od on o.id=od.orderId " +
            " where od.productId=?2 and o.customerId=?1 and o.orderStatusId =4")
    Long countOrder(Long accountId,Long productId);
}
