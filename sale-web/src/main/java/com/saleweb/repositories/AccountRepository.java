package com.saleweb.repositories;

import com.saleweb.domains.Account;
import com.saleweb.models.account.AccountQueryDTO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository
    extends PagingAndSortingRepository<Account, Long>, JpaSpecificationExecutor<Account> {
    Boolean existsByUserName(String username);

    Optional<Account> findByUserName(String userName);

    Optional<Account> findByUserNameAndPassword(String userName,String password);


  List<Account> findAllByIdIn(List<Long> ids);
}
