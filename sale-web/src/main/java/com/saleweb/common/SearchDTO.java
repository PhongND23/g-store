package com.saleweb.common;

import lombok.*;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SearchDTO {
  private Long categoryId;
  private Long accountId;
  private String name;
  private String fullname;
  private String userName;
  private String email;
  private Boolean status;
  private String type;
  private String address;
  private String phoneNumber;
  private LocalDate dateOfBirth;
  private String gender;
  private Double fromPrice;
  private Double toPrice;
  private String title;
  private Boolean myself;
  private Long roleId;
  private Long customerId;
  private Long sellerId;
  private Long orderStatusId;
  private Long id;
  private Boolean isHot;

  public SearchDTO(String userName) {
    this.setUserName(userName);
  }

  public SearchDTO(
      String fullname,
      String address,
      Boolean status,
      String email,
      String phoneNumber,
      String gender,
      LocalDate dateOfBirth) {
    this.setFullname(fullname);
    this.setAddress(address);
    this.setEmail(email);
    this.setStatus(status);
    this.setPhoneNumber(phoneNumber);
    this.setGender(gender);
    this.setDateOfBirth(dateOfBirth);
  }
}
