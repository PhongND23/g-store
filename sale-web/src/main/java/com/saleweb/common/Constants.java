package com.saleweb.common;

public class Constants {

  public static final String MESSAGE_SUCCESS="Tạo mới thành công";
  public static final long ADMIN_ROLE_ID=1L;
  public static final long MANAGE_PRODUCT_ROLE_ID=3L;


  private Constants() {
    throw new IllegalStateException("Constant class");
  }
}
